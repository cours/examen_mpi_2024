
# Calcul parallèle et distribué, grilles de calcul 

<p style="font-weight:bold; text-align: center;">(lundi 29 mai 2024, durée 2h00)</p>

• Documents de cours autorisés.
• Faites l’exercice 1 sur une copie séparée.
• Le barème est donné à titre indicatif.

## Introspection 

 Juste après avoir initialisé MPI, on procède souvent à une sorte d’“introspection”.

### Questions (2 points)

1. Quelles fonctions appelle-t-on lors de cette introspection ? Quelles variables obtient-on ? Que représentent-elles exactement ?

## Communications collectives 

Les communications collectives concernent tous les processus d’un communicateur donné.
Supposons que nous avons 10 processus et que nous avons créé un communicateur `comm_hb` comme suit :

```C
int color;
if(rank<5) color=0;
else color=1
MPI_Comm_split(MPI_COMM_WORLD, color,0, &comm_hb);
```
#### Questions (2 points)

2. Comment peut-on obtenir le rang d’un processus, que l’on nommera `rank_hb`, dans le communicateur `comm_hb` ? Quelles sont les valeurs, minimale et maximale, que peut prendre `rank_hb` ? Commentez.

On execute maintenant les instructions suivantes:

```C
if(rank_hb==0) value = rank;
MPI_Bcast(&value,1,MPI_INT,0,comm_hb);
```

#### Questions (2 points)

3. Toujours en supposant que 10 processus aient été lancés, quelle valeur contient la variable value du processus de rang `rank=2` ? de rang `rank=6` ? Commentez.

## Communications collectives

Supposons que vous avez, suite à la lecture sur un fichier (faite par le processus 0), l'array `A[i]` de taille `N`. 

#### Question (2 points)

4. Comment faites-vous pour **distribuer** `A` sur tous les processus de façon à ce que la version locale de `A`, `A_loc` soit de taille `N_loc = N/nb_proc`.   
(On suppose que `N` est divisible par `nb_proc`).   
Écrire l'appel à la fonction et commenter.

## Types Dérivés

Soit un tableau à 2 dimensions `A[i][j]` de taille N × M, manipulé par deux processus MPI.

#### Questions (4 points)

5. Comment déﬁnit-on le type MPI `type_col` permettant l’envoi de colonnes de `A` ?
6. Écrire les instructions pour envoyer la première colonne de `A` du processus 0, et la réceptionner dans la dernière colonne de `A` du processus 1, et inversement.

7. Écrire de façon schématique comment envoyer avec MPI un tableau T dont les éléments ont la structure suivante :

```C
struct particle{
    float mass ;
    float coord[3];
    int color ;
}
```

## OpenMP (4 points)

8. Paralléliser le code [exo_omp.c](exo_omp.c) avec OpenMP.
En lançant le code parallélisé, la valeur de la variable `sum` doit être affiché, ainsi le nombre de threads utilisés et le temps de calcul.

## Mesure de performance

La loi de Amdahl dit que le speed-up dû à une parallélisation est dans le cas idéal :
$$S(p) = \frac{T(1)}{T(p)} = \frac{1}{f+\frac{1-f}{p}}$$ où :
* `p` est le nombre de processus.
* `f` est la fraction de code séquentielle (pendant une execution séquentielle `f` est la fraction temps passé dans la partie du code qui ne sera pas parallèle).

#### Questions (4 points)

9. Le tableau ici représente les valeurs obtenus en tournant le code [amdahl.cpp](amdahl.cpp) sur une machine avec 16 coeurs.
Chaque ligne correspond à un nombre de threads différents.

|nth |t_seq  |t_par | t_tot |
|------|:--|:--|:---|
| 01 | 0.576942 | 0.536891 |  1.113833 |
| 02 | 0.554309 | 0.279046 |  0.833355 |
| 04 | 0.552986 | 0.151398 |  0.704384 |
| 08 | 0.555036 | 0.080265 |  0.635301 |
| 16 | 0.557901 | 0.085107 |  0.643008 |
| 32 | 0.589503 | 0.088035 |  0.677538 |

Faire le graphique de la loi d'Amdahl pour les valeurs fournis et commenter.
