#include <stdio.h>

int main() {
    int rows = 100;
    int cols = 100;
    int matrix[rows][cols];
    int sum = 0;
    int nthread = 1;

    // Initialisation de la matrice
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            matrix[i][j] = i + j;
        }
    }

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            sum += matrix[i][j];
        }
    }

    printf("En utilisant %d thread La somme des éléments de la matrice est: %d\n", nthread,sum);

    return 0;
}
