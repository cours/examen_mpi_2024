#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <random>

double my_rand(double min, double max){
  double range = (max - min);
  double div = RAND_MAX / range;
  return min + (rand() / div);
}

int main() {
    long int rows = 10000000;
    double *array;
    double maxim = 0;
    int nthreads = 1;
    double t0,t1,t2,t_seq,t_par;


    std::default_random_engine generator;
    std::normal_distribution<double> distribution(1000,1000.0);


    t0 = omp_get_wtime();

    array = (double *) malloc(rows*sizeof(double));


    for (int i = 0; i < rows; i++) {
      array[i] = distribution(generator);
    }


    t1 = omp_get_wtime();



    #pragma omp parallel
    {  // Initialisation de la matrice
        #pragma omp first
        nthreads = omp_get_num_threads();
    }

    for(int j = 0; j<100; j++){
     #pragma omp parallel for reduction(max:maxim)

        for (int i = 0; i < rows; i++) {
                if(array[i]> maxim) maxim = array[i];
        }
    }

    //printf("%f \n",maxim);

    t2 = omp_get_wtime();

    t_seq = t1 - t0;
    t_par = t2 - t1;

    printf("nth = %02d t_seq = %f t_par = %f  t_tot = %f max = %g\n", nthreads, t_seq,t_par,t2-t0,maxim);

    return 0;
}
